const express = require('express'); // load express module
const bodyParser = require('body-parser'); // load body-parser module

const app = express(); // initialize express app


// mongodb connection

// const mongoose = require('mongoose');
// const url = 'mongodb://localhost:27017/lms';

// mongoose.connect(url);

// const db = mongoose.connection;

// db.on('open',() => {
//     console.log('connected');
// });


// sql connection 

const dbConfig = require('./config/connection.js');




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


const Books = require('./routes/books'); // load routes
app.use('/books',Books) // call route with books

app.listen(4000,()=> {
    console.log('server is running'); //listen server request to use 
})
