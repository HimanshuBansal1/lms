const Book = require('../models/books');
const BookService = require('../services/books');
const Email = require('../email/sendEmail');

// create a Book api
exports.book_create = async (req,res) => {
    const createResult = BookService.create(req, (err,response) => {
        if(err) {
            console.log("error: ", err);
            return res.status(500).send("Something Went Wrong !!!");
            }
        else{
            console.log(response);
            res.send(response);
        }
    });
}

//get all Book api
exports.book_all = async function(req,res){
    try{
        const getResult = await BookService.book_all((err,response) => {
            if(err) {
                console.log("error: ", err);
                return res.status(500).send("Something Went Wrong !!!");
              }
              else{
                console.log(response);
                res.send(response);
              }
        });
    }
    catch(err){
        res.send("error occured" + err);
    }
}

//get a Book api
exports.book_details = async function(req,res){
    try{
        const getResult = await BookService.read(req.params.id,(err,response) => {
            if(err) {
                console.log("error: ", err);
                return res.status(500).send("Something Went Wrong !!!");
              }
              else{
                console.log(response);
                res.send(response);
              }
        });
    }
    catch(err){
        res.send("error occured" + err);
    }
}


// update a Book api 
exports.book_update = async function(req,res){
    try{
        const updateResult = await BookService.update(req.params.id, {$set : req.body},(err,response) => {
            if(err) {
                console.log("error: ", err);
                return res.status(500).send("Something Went Wrong !!!");
            }
            else{
                console.log(response);
                res.send(response);
            }
        });

        //send email after successfull update
        const e = await Email.sendEmail(updateResult);
        console.log(e);

    }
    catch(err){
        res.send("error occured" + err);
    }
}

//delete a Book

exports.book_delete = async function(req,res){
    try{
        const deleteResult = await BookService.delete(req.params.id,(err,response) => {
            if(err) {
                console.log("error: ", err);
                return res.status(500).send("Something Went Wrong !!!");
            }
            else{
                console.log(response);
                res.send('record deleted');
            }
        });
    }
    catch(err){
        res.send("error occured" + err);
    }
}