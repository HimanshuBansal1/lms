var nodemailer = require('nodemailer');
const config = require("config");

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.get("EMAIL_SERVICE").EMAIL,
      pass: config.get("EMAIL_SERVICE").PASSWORD,
    }
  });

module.exports.sendEmail = (updateResult) => {
    return transporter.sendMail({
        from: config.get("EMAIL_SERVICE").EMAIL,
        to: 'himanshu.bansal@unthinkable.co',
        subject: 'Email test for Node.js',
        text: "Updated Data is : " + updateResult
    });
  };
  