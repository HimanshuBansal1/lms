const mongoose = require('mongoose')

const Schema = mongoose.Schema;

let BookSchema = new Schema({
    name : {
        type : String,
        required : true,
        max : 100
    },
    author : {
        type : String,
        required : true,
        max : 100
    },
    subject : {
        type : String,
        required : true,
        max : 100
    },
})

// export the model 

module.exports = mongoose.model('Book',BookSchema);
