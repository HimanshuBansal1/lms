const express = require('express');
const router = express.Router();


const book_controller = require('../controllers/books'); 

router.get('/',book_controller.book_all); // get by id 
router.get('/:id',book_controller.book_details); // get by id 
router.post('/create',book_controller.book_create); // create a book
router.put('/:id/update', book_controller.book_update); // update a book
router.delete('/:id/delete', book_controller.book_delete); // delete a book


module.exports = router;

