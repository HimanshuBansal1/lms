const Book = require('../models/books');
const dbConfig = require('../config/connection.js');
const res = require('express/lib/response');

// create
exports.create = (req,cb) => {
    // const book = new Book(
    //     {
    //         id : req.body.id,
    //         name : req.body.name,
    //         author : req.body.author,
    //         subject : req.body.subject
    //     }
    // );
    
    // return book.save();

    dbConfig.query("INSERT INTO books(name, author,subject) VALUES(?,?,?)",
    [req.body.name, req.body.author,req.body.subject]), function(err, result){
        console.log('hgfhgvhgv');
        if(err) {
            console.log("error: ", err);
            return res.send("Something Went Wrong !!!");
          }
          else{
            console.log(result);
            return res.send(result);
          }
    };
}

// read all books
exports.book_all = function(cb){
    // return Book.find();

    dbConfig.query("Select * from books", function (err, res) {
           cb(err,res)
    });
        
}

// read
exports.read = function(bookId,cb){
    // return Book.findById(bookId);

    dbConfig.query("Select * from books where id = ?",[bookId], function (err, res) {
        cb(err,res)
    });
}

// update
exports.update = function(bookId,book,cb){
    // return Book.findByIdAndUpdate(bookId,book,{new : true}).select({_id :0 , name:1 , author: 1, subject : 1});

    dbConfig.query("UPDATE books SET name=?,author=?,subject=? WHERE id = ?", [book.name,book.author,book.subject,bookId], function(err, res) {
       cb(err,res);
    });
}

// delete 
exports.delete = function(bookId){
    // return  Book.findByIdAndDelete(bookId);

    dbConfig.query("DELETE FROM books WHERE id = ?", bookId , function(err, res) {
        if(err) {
          console.log("error: ", err);
          return res.status(500).send("Something Went Wrong !!!");
        }
        else{
          console.log('Delete Book : ', res);
          return res
        }
    });
}